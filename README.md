<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>
  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
<p align="center">
  <img src="https://img.shields.io/badge/npm-6.13.7-red" alt="NPM Version" />
  <img src="https://img.shields.io/badge/node.js-10.19.0-green" alt="NodeJS Version" />
</p>

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test

# watch mode
$ test:watch

# test coverage
$ npm run test:cov
```

---

## Entornos

Crear un archivo con la extensión **.env** dependiendo del modo de ejecución.

- development.env
- test.env

### Variables de entorno permitidas


```bash
# Application
- NODE_ENV = { development | production | test | staging }
- PORT
- LOCALES
- DEFAULTLOCALE = { es | en }

# Data Base
- MONGODB_SRV = { true | false }
- DATABASE_HOST
- DATABASE_PORT
- DATABASE_USERNAME
- DATABASE_PASSWORD
- DATABASE_NAME

# Security
- SECRET_PRIVATE_JWT
- SECRET_PUBLIC_JWT

# Token
- EXPIRES_IN_JWT = { 1d | 1h | 1m | 1s | 1ms }
- EXPIRES_IN_REFRESH_TOKEN_JWT = { 1d | 1h | 1m | 1s | 1ms }
```

Si existen dudas con respecto a las variables de entorno revisar la clase **EnvironmentVariablesService**

---

## Información de Autorización

Para la autenticación se utiliza **JWT** con llaves asimétricas **ECDSA**.

Antes de comenzar el proyecto, es necesario crear las llaves asimétricas con los siguientes comandos.

```bash
# private key
$ openssl ecparam -genkey -name secp521r1 -noout -out bdvPriKey.pem

# public key
$ openssl ec -in bdvPriKey.pem -pubout -out bdvPubkey.pem
```

Lo siguiente es agregar las variables de entorno **SECRET_PRIVATE_JWT** and **SECRET_PUBLIC_JWT** con las claves creadas, por ejemplo:

```bash
SECRET_PRIVATE_JWT = "-----BEGIN EC PRIVATE KEY-----\nMIHcAgEBBEIAr7IR7d9logeV5Tb5FS63b4y77/OdSmALrR6jjrKRrr0eLDrfP8v7\nRCRUaD+s8StCT6ncYnh5mlKDXOoDQEDLh9KgBwYFK4EEACOhgYkDgYYABAAHYmO2\nOY8GxBzzJmokOJ3K95as2aB56dIrAZUEQZ2tZvwakLwaZ3N0eVH3HsjXLp6ZZJl2\neh7m84sukJ9KN2RklwHYgc6j9FgDnIxb/+RMj9lRHcOzdF4285xXH7ffrN4bnFh+\nkNiydNAI14Qfsd/Haw/2JEeMNinuW0l9RES8zSOlIg==\n-----END EC PRIVATE KEY-----"
```

```bash
SECRET_PUBLIC_JWT = "-----BEGIN PUBLIC KEY-----\nMIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQAB2JjtjmPBsQc8yZqJDidyveWrNmg\neenSKwGVBEGdrWb8GpC8GmdzdHlR9x7I1y6emWSZdnoe5vOLLpCfSjdkZJcB2IHO\no/RYA5yMW//kTI/ZUR3Ds3ReNvOcVx+336zeG5xYfpDYsnTQCNeEH7Hfx2sP9iRH\njDYp7ltJfUREvM0jpSI=\n-----END PUBLIC KEY-----"
```

---

## Team members

- **Autor** - Erick Daniel Castellanos Flores - _erickdanielcf@gmail.com_

## Links

- [NodeJS](https://nodejs.org/es/)
- [NestJS framework documentation](https://docs.nestjs.com/)
