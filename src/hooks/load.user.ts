import { Injectable, OnApplicationBootstrap, Logger } from '@nestjs/common';
import * as fs from 'fs';
import * as i18n from 'i18n';
import { UserRegisterDTO } from '../api/database_management/dtos/user/use.register.dto';
import { EnvironmentVariablesService } from '../config/environment/environmentVariables.service';
import { UserModel } from '../api/database_management/models/user.model';

@Injectable()
export class LoadUser implements OnApplicationBootstrap {
  private readonly pathUsers = 'src/scripts/users.json';

  constructor(private readonly userModel: UserModel, private readonly config: EnvironmentVariablesService) {}

  async onApplicationBootstrap(): Promise<void> {
    if (this.config.getNodeEnv() !== 'test') {
      const aqui = await this.userModel.getCountUsers();

      if (aqui > 0) {
        Logger.log(i18n.__('seedUsers'));
        return;
      }

      Logger.log(i18n.__('seedUsersLoading'));

      if (fs.existsSync(this.pathUsers)) {
        const stringUsers: string = fs.readFileSync(this.pathUsers, 'utf8');
        const arrayUsers: string[] = stringUsers.split(/\n/);

        for (const user of arrayUsers) {
          Logger.log(user);
          const newUser: UserRegisterDTO = JSON.parse(user);

          await this.userModel.createUser(newUser);
        }
      }

      Logger.log(i18n.__('seedUsersLoaded'));
    }
  }
}
