import { Test, TestingModule } from '@nestjs/testing';
import { MemberService } from './member.service';
import { MemberModel } from '../database_management/models/member.model';
import { TeamModel } from '../database_management/models/team.model';
import { EnvironmentVariablesModule } from '../../config/environment/environmentVariables.module';
import { LanguageModule } from '../../config/languages/config/language.module';
import { MemberCreateDTO } from '../database_management/dtos/member/member.create.dto';
import { HttpStatus } from '@nestjs/common';
import { MembersGetDTO } from '../database_management/dtos/member/member.get.dto';
import { MemberEditDTO } from '../database_management/dtos/member/member.edit.dto';

const mockMemberModel = () => ({
  createMember: jest.fn(),
  findMemberById: jest.fn(),
  findMembers: jest.fn(),
  updateMemberById: jest.fn(),
  removeMemberById: jest.fn(),
});

const mockTeamModel = () => ({
  createTeam: jest.fn(),
  findTeamById: jest.fn(),
  findTeams: jest.fn(),
  updateTeamById: jest.fn(),
  removeTeamById: jest.fn(),
  modifyMember: jest.fn()
});

describe('MemberService', () => {
  let service: MemberService;
  let memberModel;
  let teamModel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EnvironmentVariablesModule,
        LanguageModule
      ],
      providers: [
        {
          provide: MemberModel,
          useFactory: mockMemberModel
        },
        {
          provide: TeamModel,
          useFactory: mockTeamModel
        },
        MemberService
      ],
    }).compile();

    service = module.get<MemberService>(MemberService);
    memberModel = module.get<MemberModel>(MemberModel);
    teamModel = module.get<TeamModel>(TeamModel);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(memberModel).toBeDefined();
    expect(teamModel).toBeDefined();
  });

  it('Register Member, error data base', async () => {
    teamModel.findTeamById.mockRejectedValue(new Error());

    const memberCreateDTO: MemberCreateDTO = {
      name: 'rogelio',
      email: 'rogelio@gmail.com',
      teamId: '5e500e7375ef086968b0a266'
    };

    try {
      await service.registerMember(memberCreateDTO);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('Register Member, team not found', async () => {
    teamModel.findTeamById.mockResolvedValue(null);

    const memberCreateDTO: MemberCreateDTO = {
      name: 'rogelio',
      email: 'rogelio@gmail.com',
      teamId: '5e500e7375ef086968b0a266'
    };

    try {
      await service.registerMember(memberCreateDTO);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.NOT_FOUND);
    }
  });

  it('Register Member, team not found', async () => {
    const responseTeamModel = {
      members: [],
      save: () => true
    }

    teamModel.findTeamById.mockResolvedValue(responseTeamModel);

    const responseMemberModel = {
      "_id": "5e50147c28910372bdcfa900",
      "creationDate": "2020-02-21T17:32:12.202Z",
      "name": "Yensi",
      "email": "anayensirc@gmail.com",
      "__v": 0
    };

    memberModel.createMember.mockResolvedValue(responseMemberModel);

    const memberCreateDTO: MemberCreateDTO = {
      name: 'rogelio',
      email: 'rogelio@gmail.com',
      teamId: '5e500e7375ef086968b0a266'
    };

    const registerMember = await service.registerMember(memberCreateDTO);

    expect(registerMember).toEqual(responseMemberModel);
  });

  it('Get Member, error data base', async () => {
    memberModel.findMemberById.mockRejectedValue(new Error());

    try {
      await service.getMember('5e500e7375ef086968b0a266');
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('Get Member, not fount', async () => {
    memberModel.findMemberById.mockResolvedValue(null);

    try {
      await service.getMember('5e500e7375ef086968b0a266');
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.NOT_FOUND);
    }
  });

  it('Get Member', async () => {

    const resolvedMember = {
      "_id": "5e50147c28910372bdcfa900",
      "creationDate": "2020-02-21T17:32:12.202Z",
      "name": "Yensi",
      "email": "anayensirc@gmail.com",
      "__v": 0
    };

    memberModel.findMemberById.mockResolvedValue(resolvedMember);

    const result = await service.getMember('5e50147c28910372bdcfa900');

    expect(result).toEqual(resolvedMember);
  });

  it('Get Members, error database', async () => {
    memberModel.findMembers.mockRejectedValue(new Error());

    const query: MembersGetDTO = {
      limit: 2,
      page: 1
    };

    try {
      await service.getMembers(query);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('Get Members', async () => {

    const responsefindMembers = [
      {
        total: { _id: null, total: 78 },
        membersData: [
          {
            "_id": "5e50147c28910372bdcfa900",
            "creationDate": "2020-02-21T17:32:12.202Z",
            "name": "Yensi",
            "email": "anayensirc@gmail.com",
            "__v": 0
          },
          {
            "_id": "0000147c28910372bdcf0000",
            "creationDate": "2020-02-21T17:32:12.202Z",
            "name": "erick",
            "email": "erick@gmail.com",
            "__v": 0
          }
        ]
      }
    ];

    memberModel.findMembers.mockResolvedValue(responsefindMembers);

    const query: MembersGetDTO = {
      limit: 2,
      page: 1
    };

    const result = await service.getMembers(query);

    const responseExpect = {
      pagination: {
        total: responsefindMembers[0].total.total,
        limit: query.limit,
        page: query.page,
        offset: query.limit * (query.page - 1)
      },
      members: responsefindMembers[0].membersData
    };

    expect(result).toEqual(responseExpect);
  });

  it('Edit Member, team error data base', async () => {
    teamModel.findTeamById.mockRejectedValue(new Error());

    const edit: MemberEditDTO = {
      name: 'modificado',
      teamId: '5e50147c28910372bdcfa900'
    };

    try {
      await service.editMember('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  });

  it('Edit member, team not found', async () => {
    teamModel.findTeamById.mockResolvedValue(null);

    const edit: MemberEditDTO = {
      name: 'modificado',
      teamId: '5e50147c28910372bdcfa900'
    };

    try {
      await service.editMember('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.NOT_FOUND);
    }

  });

  it('Edit member, team modify error data base', async () => {
    teamModel.findTeamById.mockResolvedValue(true);

    teamModel.modifyMember.mockRejectedValue(new Error());

    const edit: MemberEditDTO = {
      name: 'modificado',
      teamId: '5e50147c28910372bdcfa900'
    };

    try {
      await service.editMember('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  });

  it('Edit member, error data base', async () => {
    teamModel.findTeamById.mockResolvedValue(true);

    teamModel.modifyMember.mockResolvedValue(true);

    memberModel.updateMemberById.mockRejectedValue(new Error());

    const edit: MemberEditDTO = {
      name: 'modificado',
      teamId: '5e50147c28910372bdcfa900'
    };

    try {
      await service.editMember('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  });

  it('Edit member, not found', async () => {
    teamModel.findTeamById.mockResolvedValue(true);

    teamModel.modifyMember.mockResolvedValue(true);

    memberModel.updateMemberById.mockResolvedValue(null);

    const edit: MemberEditDTO = {
      name: 'modificado',
      teamId: '5e50147c28910372bdcfa900'
    };

    try {
      await service.editMember('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.NOT_FOUND);
    }

  });

  it('Edit member, not found', async () => {
    teamModel.findTeamById.mockResolvedValue(true);

    teamModel.modifyMember.mockResolvedValue(true);

    const memberModify = {
      "_id": "5e50147c28910372bdcfa900",
      "creationDate": "2020-02-21T17:32:12.202Z",
      "name": "modificado",
      "email": "anayensirc@gmail.com",
      "__v": 0
    };

    memberModel.updateMemberById.mockResolvedValue(memberModify);

    const edit: MemberEditDTO = {
      name: 'modificado',
      teamId: '0000147c11110372bdcfa900'
    };

    const result = await service.editMember('5e500e7375ef086968b0a266', edit);

    expect(result).toEqual(memberModify);

  });

  it('Delte Member, error database', async () => {
    memberModel.removeMemberById.mockRejectedValue(new Error());

    try {
      await service.deleteMember('5e500e7375ef086961111266');
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  });

  it('Delte Member, error database', async () => {
    memberModel.removeMemberById.mockResolvedValue(true);

    const spyService = jest.spyOn(service, 'deleteMember');
    const spy = jest.spyOn(memberModel, 'removeMemberById');

    await service.deleteMember('5e500e7375ef086961111266');

    expect(spyService).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });
});
