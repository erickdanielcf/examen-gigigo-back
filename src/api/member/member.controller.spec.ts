import { Test, TestingModule } from '@nestjs/testing';
import { MemberController } from './member.controller';
import { MemberService } from './member.service';
import { MemberCreateDTO } from '../database_management/dtos/member/member.create.dto';
import { MembersGetDTO } from '../database_management/dtos/member/member.get.dto';
import { MemberEditDTO } from '../database_management/dtos/member/member.edit.dto';

const mockMemberService = () => ({
  registerMember: jest.fn(),
  getMember: jest.fn(),
  getMembers: jest.fn(),
  editMember: jest.fn(),
  deleteMember: jest.fn()
});

describe('Member Controller', () => {
  let controller: MemberController;
  let memberService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MemberController],
      providers: [
        {
          provide: MemberService,
          useFactory: mockMemberService
        }
      ]
    }).compile();

    controller = module.get<MemberController>(MemberController);
    memberService = module.get<MemberService>(MemberService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(memberService).toBeDefined();
  });

  it('Register Member', async () => {
    const resolvedMember = {
      "creationDate": "2020-02-21T17:32:12.202Z",
      "_id": "5e50147c28910372bdcfa900",
      "name": "Yensi",
      "email": "anayensirc@gmail.com",
      "__v": 0
    };

    memberService.registerMember.mockResolvedValue(resolvedMember);

    const memberCreateDTO: MemberCreateDTO = {
      "name": "Yensi",
      "email": "anayensirc@gmail.com",
      "teamId": "5e4fa852c64e1d1bfdd0572b"
    };
    const result = await controller.registerMember(memberCreateDTO);
    const resultExpect = {
      member: resolvedMember
    };

    expect(result).toEqual(resultExpect);
  });

  it('Get Member', async () => {
    const resolvedMember = {
      "creationDate": "2020-02-21T17:32:12.202Z",
      "_id": "5e50147c28910372bdcfa900",
      "name": "Yensi",
      "email": "anayensirc@gmail.com",
      "__v": 0
    };

    memberService.getMember.mockResolvedValue(resolvedMember);

    const result = await controller.getMember('5e50147c28910372bdcfa900');
    const resultExpect = {
      member: resolvedMember
    };

    expect(result).toEqual(resultExpect);
  });

  it('Get Members', async () => {
    const resolvedMember = [
      {
        "_id": "5e50147c28910372bdcfa900",
        "creationDate": "2020-02-21T17:32:12.202Z",
        "name": "Yensi",
        "email": "anayensirc@gmail.com",
        "__v": 0
      },
      {
        "_id": "5e4fa861c64e1d1bfdd0572c",
        "creationDate": "2020-02-21T09:52:07.224Z",
        "name": "erick daniel",
        "email": "erickdaniekcf@gmail.com",
        "__v": 0
      }
    ];

    memberService.getMembers.mockResolvedValue(resolvedMember);

    const pagination: MembersGetDTO = {
      limit: 2,
      page: 1
    }

    const result = await controller.getMembers(pagination);
    const resultExpect = {
      members: resolvedMember
    };

    expect(result).toEqual(resultExpect);
  });

  it('Put Member', async () => {
    const resolvedMember = {
      "_id": "5e50147c28910372bdcfa900",
      "creationDate": "2020-02-21T17:32:12.202Z",
      "name": "modificado",
      "email": "anayensirc@gmail.com",
      "__v": 0
    };

    memberService.editMember.mockResolvedValue(resolvedMember);

    const memberEdit: MemberEditDTO = {
      name: 'modificado'
    };
    const result = await controller.putMember('5e500e7375ef086961111266', memberEdit);
    const resultExpect = {
      member: resolvedMember
    };
    
    expect(result).toEqual(resultExpect);
  });

  it('Delte Member', async () => {
    memberService.deleteMember.mockResolvedValue(true);

    const spy = jest.spyOn(controller, 'deleteMember');
    const spyService = jest.spyOn(memberService, 'deleteMember');

    await controller.deleteMember('5e500e7375ef086961111266');

    expect(spy).toHaveBeenCalled();
    expect(spyService).toHaveBeenCalled();
  });
});
