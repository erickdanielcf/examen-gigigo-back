import { Controller, Post, HttpCode, HttpStatus, Body, Get, Param, Query, Put, Delete, UseGuards } from '@nestjs/common';
import { MemberService } from './member.service';
import { ParseMongoIdPipe } from '../../config/pipes/parse-mongo-id.pipe';
import { MemberCreateDTO } from '../database_management/dtos/member/member.create.dto';
import { MemberEditDTO } from '../database_management/dtos/member/member.edit.dto';
import { MembersGetDTO } from '../database_management/dtos/member/member.get.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('member')
export class MemberController {

  constructor(private readonly memberService: MemberService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  async registerMember(@Body() memberDTO: MemberCreateDTO) {
    const member = await this.memberService.registerMember(memberDTO);
    return { member };
  }

  @Get(':memberId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async getMember(@Param('memberId', new ParseMongoIdPipe()) memberId: string) {
    const member = await this.memberService.getMember(memberId);
    return { member };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async getMembers(@Query() query: MembersGetDTO) {
    const members = await this.memberService.getMembers(query);
    return { members };
  }

  @Put(':memberId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async putMember(@Param('memberId', new ParseMongoIdPipe()) memberId: string, @Body() memberDTO: MemberEditDTO) {
    const member = await this.memberService.editMember(memberId, memberDTO);
    return { member };
  }

  @Delete(':memberId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteMember(@Param('memberId', new ParseMongoIdPipe()) memberId: string) {
    await this.memberService.deleteMember(memberId);
  }

}
