import { Module } from '@nestjs/common';
import { MemberController } from './member.controller';
import { MemberService } from './member.service';
import { DatabaseManagementModule } from '../database_management/database.management.module';

@Module({
  imports: [DatabaseManagementModule],
  controllers: [MemberController],
  providers: [MemberService]
})
export class MemberModule {}
