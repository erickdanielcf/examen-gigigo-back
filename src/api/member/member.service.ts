import { Injectable, Logger } from '@nestjs/common';
import { InternalServerErrorException } from '../../config/exceptions/internal.server.error.exception';
import { NotFoundException } from '../../config/exceptions/not.found.exception';
import { Query } from '../../common/tools/query';
import { MemberCreateDTO } from '../database_management/dtos/member/member.create.dto';
import { MembersGetDTO } from '../database_management/dtos/member/member.get.dto';
import { MemberEditDTO } from '../database_management/dtos/member/member.edit.dto';
import { MemberModel } from '../database_management/models/member.model';
import { TeamModel } from '../database_management/models/team.model';
import { Team } from '../database_management/interfaces/team/team.interface';

@Injectable()
export class MemberService {
  private logger: Logger = new Logger('MemberService');

  constructor(private readonly memberModel: MemberModel, private readonly teamModel: TeamModel) {}

  /**
   * register member
   * @param member
   * @returns new member
   */
  async registerMember(member: MemberCreateDTO): Promise<object> {

    let team: Team;

    try {
      team = await this.teamModel.findTeamById(member.teamId, false);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    if (!team) {
      throw new NotFoundException('notFoundTeam');
    }

    let response;

    try {
      response = await this.memberModel.createMember(member);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    team.members.push(response._id);

    try {
      await team.save();
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    return response;
  }

  /**
   * find member by id
   * @param memberId
   * @returns member
   */
  async getMember(memberId: string): Promise<object> {
    let response;

    try {
      response = await this.memberModel.findMemberById(memberId);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    if (!response) {
      throw new NotFoundException('notFound');
    }

    return response;
  }

  /**
   * obtain all members into data base
   * @param query
   * @returns
   */
  async getMembers(query: MembersGetDTO): Promise<object> {
    const aggregate: any[] = Query.createAggregationPipeline(query);
    let members: any[];

    try {
      members = await this.memberModel.findMembers(aggregate);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    const response = {
      pagination: {
        total: 0,
        limit: query.limit,
        page: query.page,
        offset: query.limit * (query.page - 1)
      },
      members: []
    };

    if (members[0].membersData.length > 0) {
      response.pagination.total = members[0].total.total;
      response.members = members[0].membersData;

      return response;
    }

    return response;
  }

  /**
   * modify member
   * @param memberId
   * @param memberDTO
   * @returns member
   */
  async editMember(memberId: string, memberDTO: MemberEditDTO): Promise<object> {
    let team: Team;

    if (memberDTO.teamId) {
      try {
        team = await this.teamModel.findTeamById(memberDTO.teamId, false);
      } catch (error) {
        this.logger.error(error);
        throw new InternalServerErrorException('unexpectedErrorDataBase');
      }
  
      if (!team) {
        throw new NotFoundException('notFoundTeam');
      }

      try {
        await this.teamModel.modifyMember(memberId, memberDTO.teamId);
      } catch (error) {
        this.logger.error(error);
        throw new InternalServerErrorException('unexpectedErrorDataBase');
      }
    }


    let member;

    try {
      member = await this.memberModel.updateMemberById(memberId, memberDTO);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }
    
    if (!member) {
      throw new NotFoundException('notFound');
    }

    return member;
  }

  /**
   * delete member
   * @param memberId 
   */
  async deleteMember(memberId: string): Promise<void> {
    try {
      await this.memberModel.removeMemberById(memberId);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }
  }

}
