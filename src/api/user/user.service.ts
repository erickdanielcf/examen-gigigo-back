import { Injectable } from '@nestjs/common';
import { UserModel } from '../database_management/models/user.model';
import { UserRegisterDTO } from '../database_management/dtos/user/use.register.dto';
import { UserLoginDTO } from '../database_management/dtos/user/user.login.dto';
import { UnauthorizedException } from '../../config/exceptions/unauthorized.exception';
import { compare } from 'bcrypt';
import { AuthenticationService } from '../../common/authentication/authentication.service';

@Injectable()
export class UserService {
  constructor(private readonly userModel: UserModel, private readonly authService: AuthenticationService) { }

  /**
   * register user
   * @param userDTO
   * @returns object
   */
  async registerUser(userDTO: UserRegisterDTO): Promise<object> {

    const user = await this.userModel.createUser(userDTO);

    const response = {
      _id: user._id,
      name: user.name,
      email: user.email,
      access_token: await this.authService.login(user)
    };

    return response;
  }

  /**
   * login user
   * @param userDTO
   * @returns object
   */
  async loginUser(userDTO: UserLoginDTO): Promise<object> {
    const user = await this.userModel.findUserByEmail(userDTO.email);

    if (!user) {
      throw new UnauthorizedException('unauthorized');
    }

    if (!(await compare(userDTO.password, user.password))) {
      throw new UnauthorizedException('unauthorized');
    }

    const response = {
      _id: user._id,
      name: user.name,
      email: user.email,
      access_token: await this.authService.login(user)
    };

    return response;
  }
}
