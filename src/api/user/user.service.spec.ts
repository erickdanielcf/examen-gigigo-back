import * as bcrypt from 'bcrypt';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { UserService } from './user.service';
import { UserModel } from '../database_management/models/user.model';
import { UserLoginDTO } from '../database_management/dtos/user/user.login.dto';
import { UserRegisterDTO } from '../database_management/dtos/user/use.register.dto';
import { SALT_ROUNDS_HASH_PASSWORD } from '../../common/constants';
import { JwtStrategy } from '../../common/authentication/jwt.strategy';
import { AuthenticationService } from '../../common/authentication/authentication.service';
import { LanguageModule } from '../../config/languages/config/language.module';
import { EnvironmentVariablesModule } from '../../config/environment/environmentVariables.module';
import { EnvironmentVariablesService } from '../../config/environment/environmentVariables.service';

const privateK =
  '-----BEGIN EC PRIVATE KEY-----\nMIHcAgEBBEIAr7IR7d9logeV5Tb5FS63b4y77/OdSmALrR6jjrKRrr0eLDrfP8v7\nRCRUaD+s8StCT6ncYnh5mlKDXOoDQEDLh9KgBwYFK4EEACOhgYkDgYYABAAHYmO2\nOY8GxBzzJmokOJ3K95as2aB56dIrAZUEQZ2tZvwakLwaZ3N0eVH3HsjXLp6ZZJl2\neh7m84sukJ9KN2RklwHYgc6j9FgDnIxb/+RMj9lRHcOzdF4285xXH7ffrN4bnFh+\nkNiydNAI14Qfsd/Haw/2JEeMNinuW0l9RES8zSOlIg==\n-----END EC PRIVATE KEY-----';
const publicK =
  '-----BEGIN PUBLIC KEY-----\nMIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQAB2JjtjmPBsQc8yZqJDidyveWrNmg\neenSKwGVBEGdrWb8GpC8GmdzdHlR9x7I1y6emWSZdnoe5vOLLpCfSjdkZJcB2IHO\no/RYA5yMW//kTI/ZUR3Ds3ReNvOcVx+336zeG5xYfpDYsnTQCNeEH7Hfx2sP9iRH\njDYp7ltJfUREvM0jpSI=\n-----END PUBLIC KEY-----';
const mockEnvironmentVariablesService = () => ({
  getSecretPrivateJWT: jest.fn().mockResolvedValue(privateK),
  getSecretPublicJWT: jest.fn().mockResolvedValue(publicK)
});

const mockUserModel = () => ({
  createUser: jest.fn(),
  findUserByEmail: jest.fn()
});
const mockAuthenticationService = () => ({
  login: jest.fn(),
  validateUser: jest.fn()
});

describe('User Service', () => {
  let userService;
  let userModel;
  let auth;
  let jwt;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.registerAsync({
          imports: [EnvironmentVariablesModule],
          inject: [EnvironmentVariablesService],
          useFactory(config: EnvironmentVariablesService) {
            return {
              secret: config.getSecretPrivateJWT(),
              signOptions: { expiresIn: config.getExpiresInJWT() }
            };
          }
        }),
        LanguageModule
      ],
      providers: [
        JwtStrategy,
        {
          provide: EnvironmentVariablesService,
          useFactory: mockEnvironmentVariablesService
        },
        {
          provide: AuthenticationService,
          useFactory: mockAuthenticationService
        },
        {
          provide: UserModel,
          useFactory: mockUserModel
        },
        UserService
      ]
    }).compile();

    userService = module.get<UserService>(UserService);
    userModel = module.get<UserModel>(UserModel);
    auth = module.get<AuthenticationService>(AuthenticationService);
    jwt = module.get<JwtService>(JwtService);
  });

  it('Should be defined', () => {
    expect(userService).toBeDefined();
    expect(userModel).toBeDefined();
    expect(auth).toBeDefined();
    expect(jwt).toBeDefined();
  });

  it('Register User', async () => {
    const payload = {
      name: 'Erick',
      email: 'erickdanielcf@gmail.com'
    };
    const resolvedUser = {
      name: payload.name,
      email: payload.email,
      access_token: jwt.sign(payload)
    };

    userModel.createUser.mockResolvedValue(resolvedUser);
    auth.login.mockResolvedValue(jwt.sign(payload));

    const registerUser: UserRegisterDTO = {
      name: 'Erick',
      email: 'erickdanielcf@gmail.com',
      password: 'password'
    };

    const result = await userService.registerUser(registerUser);

    expect(result).toEqual(resolvedUser);
  });

  it('Login, email User not found', async () => {
    userModel.findUserByEmail.mockResolvedValue(null);

    const loginUser: UserLoginDTO = {
      email: 'erickdanielcf@gmail.com',
      password: 'password'
    };

    try {
      await userService.loginUser(loginUser);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.UNAUTHORIZED);
    }
  });

  it('Login, incorrect password User', async () => {
    const resolvedUser = {
      name: 'Erick',
      email: 'erickdanielcf@gmail.com',
      password: await bcrypt.hash('password', SALT_ROUNDS_HASH_PASSWORD)
    };
    userModel.findUserByEmail.mockResolvedValue(resolvedUser);

    const loginUser: UserLoginDTO = {
      email: 'erickdanielcf@gmail.com',
      password: 'password1'
    };

    try {
      await userService.loginUser(loginUser);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.UNAUTHORIZED);
    }
  });

  it('Login User', async () => {
    const payload = {
      name: 'Erick',
      email: 'erickdanielcf@gmail.com'
    };

    const findUser = {
      name: payload.name,
      email: payload.email,
      password: await bcrypt.hash('password', SALT_ROUNDS_HASH_PASSWORD)
    };
    userModel.findUserByEmail.mockResolvedValue(findUser);
    auth.login.mockResolvedValue(jwt.sign(payload));

    const loginUser: UserLoginDTO = {
      email: 'erickdanielcf@gmail.com',
      password: 'password'
    };

    const result = await userService.loginUser(loginUser);

    const resolvedUser = {
      name: payload.name,
      email: payload.email,
      access_token: jwt.sign(payload)
    };

    expect(result).toEqual(resolvedUser);
  });
});
