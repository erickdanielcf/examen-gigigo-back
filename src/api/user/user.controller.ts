import { Controller, Post, Body, HttpCode, HttpStatus } from '@nestjs/common';
import { UserService } from './user.service';
import { UserRegisterDTO } from '../database_management/dtos/user/use.register.dto';
import { UserLoginDTO } from '../database_management/dtos/user/user.login.dto';

@Controller('auth')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  async register(@Body() userDTO: UserRegisterDTO) {
    const user = await this.userService.registerUser(userDTO);
    return user;
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() userDTO: UserLoginDTO) {
    const user = await this.userService.loginUser(userDTO);
    return user;
  }

  /*@Roles('adminBar') // '', ''
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post('prueba')
  async prueba() {
    return 'prueba superada';
  }*/
}
