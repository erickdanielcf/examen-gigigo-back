import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { EnvironmentVariablesModule } from '../../config/environment/environmentVariables.module';
import { EnvironmentVariablesService } from '../../config/environment/environmentVariables.service';
import { LanguageModule } from '../../config/languages/config/language.module';
import { UserRegisterDTO } from '../database_management/dtos/user/use.register.dto';
import { UserLoginDTO } from '../database_management/dtos/user/user.login.dto';

const mockUserService = () => ({
  registerUser: jest.fn(),
  loginUser: jest.fn()
});

describe('User Controller', () => {
  let controller;
  let userService;
  let jwt;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.registerAsync({
          imports: [EnvironmentVariablesModule],
          inject: [EnvironmentVariablesService],
          useFactory(config: EnvironmentVariablesService) {
            return {
              secret: config.getSecretPrivateJWT(),
              signOptions: { expiresIn: config.getExpiresInJWT() }
            };
          }
        }),
        LanguageModule
      ],
      controllers: [UserController],
      providers: [
        {
          provide: UserService,
          useFactory: mockUserService
        }
      ]
    }).compile();

    controller = module.get<UserController>(UserController);
    userService = module.get<UserService>(UserService);
    jwt = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(userService).toBeDefined();
    expect(jwt).toBeDefined();
  });

  it('Register Controller', async () => {
    const payload = {
      name: 'Erick',
      email: 'erickdanielcf@gmail.com'
    };
    const resolvedUser = {
      name: payload.name,
      email: payload.email,
      access_token: jwt.sign(payload)
    };

    userService.registerUser.mockResolvedValue(resolvedUser);

    const userRegisterDTO: UserRegisterDTO = {
      name: 'Erick',
      email: 'erickdanielcfQgmail.com',
      password: 'password'
    };
    const result = await controller.register(userRegisterDTO);

    expect(result).toEqual(resolvedUser);
  });

  it('Login Controller', async () => {
    const payload = {
      name: 'Erick',
      email: 'erickdanielcf@gmail.com'
    };
    const resolvedUser = {
      name: payload.name,
      email: payload.email,
      access_token: jwt.sign(payload)
    };

    userService.loginUser.mockResolvedValue(resolvedUser);

    const userLoginDTO: UserLoginDTO = {
      email: 'erickdanielcfQgmail.com',
      password: 'password'
    };
    const result = await controller.login(userLoginDTO);

    expect(result).toEqual(resolvedUser);
  });
});
