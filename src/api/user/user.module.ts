import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { DatabaseManagementModule } from '../database_management/database.management.module';
import { AuthenticationModule } from '../../common/authentication/authentication.module';

@Module({
  imports: [DatabaseManagementModule, AuthenticationModule],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
