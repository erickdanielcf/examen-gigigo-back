import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';
import { MIN_PASSWORD, MAX_PASSWORD } from '../../../../common/constants';

export class UserRegisterDTO {
  @IsString()
  @MinLength(1)
  readonly name: string;

  @IsEmail()
  readonly email: string;

  @IsString()
  @MinLength(MIN_PASSWORD)
  @MaxLength(MAX_PASSWORD)
  readonly password: string;
}
