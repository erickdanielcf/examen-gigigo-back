import { IsString, MinLength, MaxLength, IsEmail } from 'class-validator';
import { MIN_PASSWORD, MAX_PASSWORD } from '../../../../common/constants';

export class UserLoginDTO {
  @IsEmail()
  readonly email: string;

  @IsString()
  @MinLength(MIN_PASSWORD)
  @MaxLength(MAX_PASSWORD)
  readonly password: string;
}
