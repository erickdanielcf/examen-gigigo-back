import { IsString, IsNotEmpty, IsEmail, IsMongoId } from 'class-validator';

export class MemberCreateDTO {
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsEmail()
  readonly email: string;

  @IsMongoId()
  readonly teamId: string
}
