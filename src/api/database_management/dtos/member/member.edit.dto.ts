import { IsString, IsOptional, IsEmail, IsMongoId } from 'class-validator';

export class MemberEditDTO {
  @IsOptional()
  @IsString()
  readonly name?: string;

  @IsOptional()
  @IsEmail()
  readonly email?: string;

  @IsOptional()
  @IsMongoId()
  readonly teamId?: string;
}
