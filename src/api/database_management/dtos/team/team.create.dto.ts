import { IsString,IsNotEmpty } from 'class-validator';

export class TeamCreateDTO {
  @IsNotEmpty()
  @IsString()
  readonly name: string;
}
