import { IsNumber } from 'class-validator';
import { Transform } from 'class-transformer';

export class TeamsGetDTO {
  @IsNumber()
  @Transform(page => {
    page = Number(page);

    if (isNaN(page)) {
      return (page = 1);
    }
    return page;
  })
  readonly page?: number = 1;

  @IsNumber()
  @Transform(limit => {
    limit = Number(limit);

    if (isNaN(limit)) {
      return (limit = 20);
    }
    return limit;
  })
  readonly limit?: number = 20;
}
