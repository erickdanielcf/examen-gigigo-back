import { IsString, IsOptional } from 'class-validator';

export class TeamEditDTO {
  @IsOptional()
  @IsString()
  readonly name?: string;
}
