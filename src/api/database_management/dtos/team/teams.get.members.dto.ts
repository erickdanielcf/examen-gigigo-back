import { IsBoolean } from 'class-validator';
import { Transform } from 'class-transformer';

export class TeamsGetMembersDTO {
  @IsBoolean()
  @Transform(showMembers => {
    showMembers = showMembers == 'true';

    return showMembers;
  })
  readonly showMembers?: boolean = false;
}
