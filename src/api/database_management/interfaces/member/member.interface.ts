import { Document } from 'mongoose';

export interface Member extends Document {
  readonly name: string;
  readonly email: string;
  readonly creationDate: Date;
}
