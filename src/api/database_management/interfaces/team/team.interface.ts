import { Document } from 'mongoose';
import { Member } from '../member/member.interface';

export interface Team extends Document {
  readonly name: string;
  readonly creationDate: Date;
  members: Member[];
}
