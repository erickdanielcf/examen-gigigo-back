import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../interfaces/user/user.interface';
import { UserRegisterDTO } from '../dtos/user/use.register.dto';

@Injectable()
export class UserModel {
  constructor(@InjectModel('user') readonly userModel: Model<User>) { }

  /**
   * obtain all total users register in data base
   * @returns number
   */
  async getCountUsers(): Promise<number> {
    const total = await this.userModel.countDocuments();

    return total;
  }

  /**
   * create user
   * @param userDTO
   * @returns user
   */
  async createUser(userDTO: UserRegisterDTO): Promise<User> {
    /*const user = new this.userModel(userDTO);
    return await user.save();*/
    const user = this.userModel.create(userDTO);
    return await user;
  }

  /**
   * findUserByEmail
   * @param email
   * @returns user
   */
  async findUserByEmail(email: string): Promise<User> {
    const user = await this.userModel.findOne({ email });

    return user;
  }

  /**
   * get users
   * @returns array users
   */
  async getUsers(): Promise<User[]> {
    const users = await this.userModel.find();

    return users;
  }
}
