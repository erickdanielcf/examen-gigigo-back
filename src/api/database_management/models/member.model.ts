import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Member } from '../interfaces/member/member.interface';
import { MemberCreateDTO } from '../dtos/member/member.create.dto';
import { MemberEditDTO } from '../dtos/member/member.edit.dto';

@Injectable()
export class MemberModel {
  constructor(@InjectModel('member') readonly memberModel: Model<Member>) {}

  /**
   * save new member into data base
   * @param memberDTO
   * @returns member
   */
  async createMember(memberDTO: MemberCreateDTO): Promise<Member> {
    const newMember = this.memberModel.create(memberDTO);
    return await newMember;
  }

  /**
   * find member by id
   * @param _id
   * @returns member
   */
  async findMemberById(_id: string): Promise<Member> {
    const member = this.memberModel.findOne({ _id });
    return await member;
  }

  /**
   * members search with pagination
   * @param queryAggregate
   * @returns array result
   */
  async findMembers(queryAggregate: any[]): Promise<any[]> {
    const aggregateQuery = [
      {
        $facet: {
          totalCount: [
            { $project: { _id: 1 } },
            { $group: { _id: null, total: { $sum: 1 } } }
          ],
          membersData: [
            { $sort: { _id: -1 } },
          ].concat(queryAggregate)
        }
      },
      { $project: { total: { $arrayElemAt: ['$totalCount', 0] }, membersData: '$membersData' } }
    ];

    const members = await this.memberModel.aggregate([aggregateQuery]);
    return members;
  }

  /**
   * update member by id
   * @param _id
   * @param memberDTO
   * @returns member
   */
  async updateMemberById(_id: string, memberDTO: MemberEditDTO): Promise<Member> {
    const member = await this.memberModel.findOneAndUpdate(
      { _id },
      memberDTO,
      { new: true }
    );

    return member;
  }

  /**
   * remove member by id
   * @param _id
   */
  async removeMemberById(_id: string): Promise<void> {
    await this.memberModel.findOneAndRemove({ _id });
  }
}
