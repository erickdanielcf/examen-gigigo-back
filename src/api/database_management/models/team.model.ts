import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Team } from '../interfaces/team/team.interface';
import { TeamCreateDTO } from '../dtos/team/team.create.dto';
import { TeamEditDTO } from '../dtos/team/team.edit.dto';

@Injectable()
export class TeamModel {
  constructor(@InjectModel('team') readonly teamModel: Model<Team>) {}

  /**
   * save new team into data base
   * @param teamDTO
   * @returns team
   */
  async createTeam(teamDTO: TeamCreateDTO): Promise<Team> {
    const newTeam = this.teamModel.create(teamDTO);
    return await newTeam;
  }

  /**
   * find team by id
   * @param _id
   * @returns team
   */
  async findTeamById(_id: string, showMembers: boolean): Promise<Team> {
    let team;

    if (showMembers) {
      team = this.teamModel.findOne({ _id })
        .populate('members', { name: 1, email: 1 });
    } else {
      team = this.teamModel.findOne({ _id });
    }
      
    return await team;
  }

  /**
   * teams search with pagination
   * @param queryAggregate
   * @returns array result
   */
  async findTeams(queryAggregate: any[], showMembers: boolean): Promise<any[]> {
    const teamsData: object[] = [
      { $sort: { _id: -1 } }
    ];

    if (showMembers) {
      teamsData.push({
        $lookup: {
          from: 'members',
          let: { fromLocal: '$members' },
          pipeline: [
            { $match: { $expr: { $in: ['$_id', '$$fromLocal'] } } },
            {
              $project: {
                _id: 1,
                name: 1,
                email: 1
              }
            }
          ],
          as: 'members'
        }
      });
    }

    const aggregateQuery = [
      {
        $facet: {
          totalCount: [
            { $project: { _id: 1 } },
            { $group: { _id: null, total: { $sum: 1 } } }
          ],
          teamsData: teamsData.concat(queryAggregate)
        }
      },
      { $project: { total: { $arrayElemAt: ['$totalCount', 0] }, teamsData: '$teamsData' } }
    ];

    const teams = await this.teamModel.aggregate(aggregateQuery);
    return teams;
  }

  /**
   * update team by id
   * @param _id
   * @param teamDTO
   * @returns team
   */
  async updateTeamById(_id: string, teamDTO: TeamEditDTO): Promise<Team> {
    const team = await this.teamModel.findOneAndUpdate(
      { _id },
      teamDTO,
      { new: true }
    );

    return team;
  }

  /**
   * remove team by id
   * @param _id
   */
  async removeTeamById(_id: string): Promise<void> {
    await this.teamModel.findOneAndRemove({ _id });
  }

  /**
   * modify member
   * @param memberId
   * @param teamId
   */
  async modifyMember(memberId: string, teamId: string): Promise<void> {
    await this.teamModel.update({ members: memberId }, { $pull: { members: memberId } });

    await this.teamModel.update({ _id: teamId }, { $push: { members: memberId } });
  }
}
