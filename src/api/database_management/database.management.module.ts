import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TeamModel } from './models/team.model';
import { TeamSchema } from './schemas/team.schema';
import { MemberSchema } from './schemas/member.schema';
import { MemberModel } from './models/member.model';
import { UserSchema } from './schemas/user.schema';
import { UserModel } from './models/user.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'team', schema: TeamSchema },
      { name: 'member', schema: MemberSchema },
      { name: 'user', schema: UserSchema },
    ])
  ],
  providers: [TeamModel, MemberModel, UserModel],
  exports: [TeamModel, MemberModel, UserModel]
})
export class DatabaseManagementModule {}
