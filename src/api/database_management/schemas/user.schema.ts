import { Schema, HookNextFunction } from 'mongoose';
import { hash } from 'bcrypt';
import * as _ from 'lodash';
import { SALT_ROUNDS_HASH_PASSWORD } from '../../../common/constants';

export const UserSchema = new Schema({
  name: { type: String, required: 'requiredField name' },
  email: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  password: { type: String, required: 'requiredField password' }
});

UserSchema.index({ email: 1 }, { unique: true });

UserSchema.method('payloadToken', function() {
  return _.pick(this, ['_id', 'name', 'email']);
});

UserSchema.method('basicUser', function() {
  return _.pick(this, ['name', 'email']);
});

UserSchema.pre('save', async function(next: HookNextFunction) {
  const user = this;

  if (user.isModified('password')) {
    user['password'] = await hash(user['password'], SALT_ROUNDS_HASH_PASSWORD);
  }

  return next();
});
