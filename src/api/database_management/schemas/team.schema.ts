import { Schema } from 'mongoose';

const memberSchema = { type: Schema.Types.ObjectId, ref: 'member' };

export const TeamSchema = new Schema({
  name: {
    type: String,
    required: 'requiredField name',
    trim: true,
  },
  creationDate: {
    type: Date,
    required: true,
    default: new Date()
  },
  members: [memberSchema]
});
