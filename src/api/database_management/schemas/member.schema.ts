import { Schema } from 'mongoose';

export const MemberSchema = new Schema({
  name: {
    type: String,
    required: 'requiredField name',
    trim: true,
  },
  email: {
    type: String,
    required: 'requiredField email',
    trim: true,
  },
  creationDate: {
    type: Date,
    required: true,
    default: new Date()
  },
});

MemberSchema.index({ email: 1 }, { unique: true, sparse: true });
