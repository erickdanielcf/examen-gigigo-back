import { Injectable, Logger } from '@nestjs/common';
import { TeamCreateDTO } from '../database_management/dtos/team/team.create.dto';
import { TeamModel } from '../database_management/models/team.model';
import { InternalServerErrorException } from '../../config/exceptions/internal.server.error.exception';
import { NotFoundException } from '../../config/exceptions/not.found.exception';
import { TeamsGetDTO } from '../database_management/dtos/team/teams.get.dto';
import { Query } from '../../common/tools/query';
import { TeamEditDTO } from '../database_management/dtos/team/team.edit.dto';

@Injectable()
export class TeamService {
  private logger: Logger = new Logger('TeamService');

  constructor(private readonly teamModel: TeamModel) {}

  /**
   * register team
   * @param team
   * @returns new team
   */
  async registerTeam(team: TeamCreateDTO): Promise<object> {
    let response;

    try {
      response = await this.teamModel.createTeam(team);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    return response;
  }

  /**
   * find team by id
   * @param teamId
   * @param showMembers
   * @returns team
   */
  async getTeam(showMembers: boolean, teamId: string): Promise<object> {
    let response;

    try {
      response = await this.teamModel.findTeamById(teamId, showMembers);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    if (!response) {
      throw new NotFoundException('notFound');
    }

    return response;
  }

  /**
   * obtain all teams into data base
   * @param query
   * @returns
   */
  async getTeams(showMembers: boolean, query?: TeamsGetDTO): Promise<object> {
    const aggregate: any[] = Query.createAggregationPipeline(query);
    let teams: any[];

    try {
      teams = await this.teamModel.findTeams(aggregate, showMembers);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }

    const response = {
      pagination: {
        total: 0,
        limit: query.limit,
        page: query.page,
        offset: query.limit * (query.page - 1)
      },
      teams: []
    };

    if (teams[0].teamsData.length > 0) {
      response.pagination.total = teams[0].total.total;
      response.teams = teams[0].teamsData;

      return response;
    }

    return response;
  }

  /**
   * modify team
   * @param teamId
   * @param teamDTO
   * @returns team
   */
  async editTeam(teamId: string, teamDTO: TeamEditDTO): Promise<object> {
    let team;

    try {
      team = await this.teamModel.updateTeamById(teamId, teamDTO);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }
    
    if (!team) {
      throw new NotFoundException('notFound');
    }

    return team;
  }

  /**
   * delete team
   * @param teamId 
   */
  async deleteTeam(teamId: string): Promise<void> {
    try {
      await this.teamModel.removeTeamById(teamId);
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException('unexpectedErrorDataBase');
    }
  }

}
