import { Test, TestingModule } from '@nestjs/testing';
import { TeamService } from './team.service';
import { TeamModel } from '../database_management/models/team.model';
import { TeamCreateDTO } from '../database_management/dtos/team/team.create.dto';
import { HttpStatus } from '@nestjs/common';
import { EnvironmentVariablesModule } from '../../config/environment/environmentVariables.module';
import { LanguageModule } from '../../config/languages/config/language.module';
import { TeamsGetDTO } from '../database_management/dtos/team/teams.get.dto';
import { TeamEditDTO } from '../database_management/dtos/team/team.edit.dto';

const mockTeamModel = () => ({
  createTeam: jest.fn(),
  findTeamById: jest.fn(),
  findTeams: jest.fn(),
  updateTeamById: jest.fn(),
  removeTeamById: jest.fn()
});

describe('TeamService', () => {
  let service: TeamService;
  let teamModel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EnvironmentVariablesModule,
        LanguageModule
      ],
      providers: [
        {
          provide: TeamModel,
          useFactory: mockTeamModel
      },
      TeamService
    ],
    }).compile();

    service = module.get<TeamService>(TeamService);
    teamModel = module.get<TeamModel>(TeamModel);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(teamModel).toBeDefined();
  });

  it('Register Team, error data base', async () => {
    teamModel.createTeam.mockRejectedValue(new Error());

    const teamCreateDTO: TeamCreateDTO = {
      name: 'equipo rogelio'
    };

    try {
      await service.registerTeam(teamCreateDTO);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('Register Team', async () => {
    const resolvedTeam = {
        "creationDate": "2020-02-21T17:08:00.350Z",
        "members": [],
        "_id": "5e500e7375ef086968b0a266",
        "name": "equipo rogelio",
        "__v": 0
    };

    teamModel.createTeam.mockResolvedValue(resolvedTeam);

    const teamCreateDTO: TeamCreateDTO = {
      name: 'equipo rogelio'
    };
    const result = await service.registerTeam(teamCreateDTO);

    expect(result).toEqual(resolvedTeam);
  });

  it('Get Team, error data base', async () => {
    teamModel.findTeamById.mockRejectedValue(new Error());

    try {
      await service.getTeam(false, '5e500e7375ef086968b0a266');
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('Get Team, not fount', async () => {
    teamModel.findTeamById.mockResolvedValue(null);

    try {
      await service.getTeam(false, '5e500e7375ef086968b0a266');
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.NOT_FOUND);
    }
  });

  it('Get Team', async () => {

    const resolvedTeam = {
      "creationDate": "2020-02-21T17:08:00.350Z",
      "members": ['11110e7375ef086968b01111'],
      "_id": "5e500e7375ef086968b0a266",
      "name": "equipo rogelio",
      "__v": 0
  };

    teamModel.findTeamById.mockResolvedValue(resolvedTeam);

    const result = await service.getTeam(false, '5e500e7375ef086968b0a266');

    expect(result).toEqual(resolvedTeam);
  });

  it('Get Teams, error database', async () => {
    teamModel.findTeams.mockRejectedValue(new Error());

    const query: TeamsGetDTO = {
      limit: 2,
      page: 1
    };

    try {
      await service.getTeams(false, query);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('Get Teams', async () => {

    const responsefindTeams = [
      {
        total: { _id: null, total: 78 },
        teamsData: [
          {
            "creationDate": "2020-02-21T17:08:00.350Z",
            "members": ['66660e7375ef086968b06666'],
            "_id": "11110e7375ef086968b01111",
            "name": "equipo erick",
            "__v": 0
          },
          {
            "creationDate": "2020-02-21T17:08:00.350Z",
            "members": ['00000e7375ef086968b00000'],
            "_id": "5e500e7375ef086968b0a266",
            "name": "equipo rogelio",
            "__v": 0
          }
        ]
      }
    ];

    teamModel.findTeams.mockResolvedValue(responsefindTeams);

    const query: TeamsGetDTO = {
      limit: 2,
      page: 1
    };

    const result = await service.getTeams(false, query);

    const responseExpect = {
      pagination: {
        total: responsefindTeams[0].total.total,
        limit: query.limit,
        page: query.page,
        offset: query.limit * (query.page - 1)
      },
      teams: responsefindTeams[0].teamsData
    };

    expect(result).toEqual(responseExpect);
  });

  it('Edit Team, error data base', async () => {
    teamModel.updateTeamById.mockRejectedValue(new Error());

    const edit: TeamEditDTO = {
      name: 'modificado'
    };

    try {
      await service.editTeam('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  });

  it('Edit Team, not found', async () => {
    teamModel.updateTeamById.mockResolvedValue(null);

    const edit: TeamEditDTO = {
      name: 'modificado'
    };

    try {
      await service.editTeam('5e500e7375ef086968b0a266', edit);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.NOT_FOUND);
    }

  });

  it('Edit Team', async () => {

    const resolvedTeam = {
      "creationDate": "2020-02-21T17:08:00.350Z",
      "members": ['11110e7375ef086968b01111'],
      "_id": "5e500e7375ef086968b0a266",
      "name": "modificado",
      "__v": 0
  };

    teamModel.updateTeamById.mockResolvedValue(resolvedTeam);

    const edit: TeamEditDTO = {
      name: 'modificado'
    };

    const result = await service.editTeam('5e500e7375ef086968b0a266', edit);

    expect(result).toEqual(resolvedTeam);
  });

  it('Delte Team, error database', async () => {
    teamModel.removeTeamById.mockRejectedValue(new Error());

    try {
      await service.deleteTeam('5e500e7375ef086961111266');
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  });

  it('Delte Team, error database', async () => {
    teamModel.removeTeamById.mockResolvedValue(true);

    const spyService = jest.spyOn(service, 'deleteTeam');
    const spy = jest.spyOn(teamModel, 'removeTeamById');

    await service.deleteTeam('5e500e7375ef086961111266');

    expect(spyService).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });
});
