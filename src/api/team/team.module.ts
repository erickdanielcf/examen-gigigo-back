import { Module } from '@nestjs/common';
import { TeamController } from './team.controller';
import { TeamService } from './team.service';
import { DatabaseManagementModule } from '../database_management/database.management.module';

@Module({
  imports: [DatabaseManagementModule],
  controllers: [TeamController],
  providers: [TeamService]
})
export class TeamModule {}
