import { Test, TestingModule } from '@nestjs/testing';
import { TeamController } from './team.controller';
import { TeamService } from './team.service';
import { TeamCreateDTO } from '../database_management/dtos/team/team.create.dto';
import { TeamsGetMembersDTO } from '../database_management/dtos/team/teams.get.members.dto';
import { TeamsGetDTO } from '../database_management/dtos/team/teams.get.dto';
import { TeamEditDTO } from '../database_management/dtos/team/team.edit.dto';

const mockTeamService = () => ({
  registerTeam: jest.fn(),
  getTeam: jest.fn(),
  getTeams: jest.fn(),
  editTeam: jest.fn(),
  deleteTeam: jest.fn()
});

describe('Team Controller', () => {
  let controller: TeamController;
  let teamService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeamController],
      providers: [
        {
          provide: TeamService,
          useFactory: mockTeamService
        }
      ]
    }).compile();

    controller = module.get<TeamController>(TeamController);
    teamService = module.get<TeamService>(TeamService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(teamService).toBeDefined();
  });

  it('Register Team', async () => {
    const resolvedTeam = {
        "creationDate": "2020-02-21T17:08:00.350Z",
        "members": [],
        "_id": "5e500e7375ef086968b0a266",
        "name": "equipo rogelio",
        "__v": 0
    };

    teamService.registerTeam.mockResolvedValue(resolvedTeam);

    const teamCreateDTO: TeamCreateDTO = {
      name: 'equipo rogelio'
    };
    const result = await controller.registerTeam(teamCreateDTO);
    const resultExpect = {
      team: resolvedTeam
    };

    expect(result).toEqual(resultExpect);
  });

  it('Get Team', async () => {
    const resolvedTeam = {
        "creationDate": "2020-02-21T17:08:00.350Z",
        "members": [],
        "_id": "5e500e7375ef086968b0a266",
        "name": "equipo rogelio",
        "__v": 0
    };

    teamService.getTeam.mockResolvedValue(resolvedTeam);

    const showMembers: TeamsGetMembersDTO = {
      showMembers: false
    };
    const result = await controller.getTeam('5e500e7375ef086968b0a266', showMembers);
    const resultExpect = {
      team: resolvedTeam
    };

    expect(result).toEqual(resultExpect);
  });

  it('Get Teams', async () => {
    const resolvedTeam = [
      {
          "creationDate": "2020-02-21T17:08:00.350Z",
          "members": [],
          "_id": "5e500e7375ef086968b0a266",
          "name": "equipo rogelio",
          "__v": 0
      },
      {
        "creationDate": "2020-02-22T17:08:00.350Z",
        "members": [],
        "_id": "5e500e7375ef086961111266",
        "name": "equipo erick",
        "__v": 0
    }
    ];

    teamService.getTeams.mockResolvedValue(resolvedTeam);

    const pagination: TeamsGetDTO = {
      limit: 2,
      page: 1
    }

    const showMembers: TeamsGetMembersDTO = {
      showMembers: false
    };
    const result = await controller.getTeams(pagination, showMembers);
    const resultExpect = {
      teams: resolvedTeam
    };

    expect(result).toEqual(resultExpect);
  });

  it('Put Team', async () => {
    const resolvedTeam = {
        "creationDate": "2020-02-22T17:08:00.350Z",
        "members": [],
        "_id": "5e500e7375ef086961111266",
        "name": "equipo modificado",
        "__v": 0
    };

    teamService.editTeam.mockResolvedValue(resolvedTeam);

    const teamEdit: TeamEditDTO = {
      name: 'equipo modificado'
    };
    const result = await controller.putTeam('5e500e7375ef086961111266', teamEdit);
    const resultExpect = {
      team: resolvedTeam
    };
    
    expect(result).toEqual(resultExpect);
  });

  it('Delte Team', async () => {
    teamService.deleteTeam.mockResolvedValue(true);

    const spy = jest.spyOn(controller, 'deleteTeam');
    const spyService = jest.spyOn(teamService, 'deleteTeam');

    await controller.deleteTeam('5e500e7375ef086961111266');

    expect(spy).toHaveBeenCalled();
    expect(spyService).toHaveBeenCalled();
  });
});
