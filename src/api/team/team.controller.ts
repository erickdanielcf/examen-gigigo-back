import { Controller, Post, HttpCode, HttpStatus, Body, Get, Param, Query, Put, Delete, UseGuards } from '@nestjs/common';
import { TeamService } from './team.service';
import { TeamCreateDTO } from '../database_management/dtos/team/team.create.dto';
import { ParseMongoIdPipe } from '../../config/pipes/parse-mongo-id.pipe';
import { TeamsGetDTO } from '../database_management/dtos/team/teams.get.dto';
import { TeamEditDTO } from '../database_management/dtos/team/team.edit.dto';
import { TeamsGetMembersDTO } from '../database_management/dtos/team/teams.get.members.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('team')
export class TeamController {

  constructor(private readonly teamService: TeamService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  async registerTeam(@Body() teamDTO: TeamCreateDTO) {
    const team = await this.teamService.registerTeam(teamDTO);
    return { team };
  }

  @Get(':teamId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async getTeam(@Param('teamId', new ParseMongoIdPipe()) teamId: string, @Query() query: TeamsGetMembersDTO) {
    const team = await this.teamService.getTeam(query.showMembers, teamId);
    return { team };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async getTeams(@Query() query: TeamsGetDTO, @Query() { showMembers }: TeamsGetMembersDTO) {
    const teams = await this.teamService.getTeams(showMembers, query);
    return { teams };
  }

  @Put(':teamId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async putTeam(@Param('teamId', new ParseMongoIdPipe()) teamId: string, @Body() teamDTO: TeamEditDTO) {
    const team = await this.teamService.editTeam(teamId, teamDTO);
    return { team };
  }

  @Delete(':teamId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteTeam(@Param('teamId', new ParseMongoIdPipe()) teamId: string) {
    await this.teamService.deleteTeam(teamId);
  }

}
