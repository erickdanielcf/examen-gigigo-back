import { Module } from '@nestjs/common';
import { LanguageModule } from './config/languages/config/language.module';
import { APP_FILTER, APP_PIPE, APP_INTERCEPTOR } from '@nestjs/core';
import { HttpExceptionFilter } from './config/exceptions/filter/http-exception.filter';
import { MongoDBExceptionFilter } from './config/exceptions/filter/mongodb-exception.filter';
import { MongooseValidationErrorFilter } from './config/exceptions/filter/mongoose-exception.filter';
import { LoggingInterceptor } from './config/interceptors/logging.interceptor';
import { ValidationPipe } from './config/pipes/validation.pipe';
import { EnvironmentVariablesModule } from './config/environment/environmentVariables.module';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseConfigService } from './config/database/mongoose-config.service';
import { TeamModule } from './api/team/team.module';
import { MemberModule } from './api/member/member.module';
import { AuthenticationModule } from './common/authentication/authentication.module';
import { UserModule } from './api/user/user.module';
import { LoadUser } from './hooks/load.user';
import { DatabaseManagementModule } from './api/database_management/database.management.module';

@Module({
  imports: [
    EnvironmentVariablesModule,
    LanguageModule,
    MongooseModule.forRootAsync({
      useClass: MongooseConfigService
    }),
    TeamModule,
    MemberModule,
    AuthenticationModule,
    UserModule,
    DatabaseManagementModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter
    },
    {
      provide: APP_FILTER,
      useClass: MongoDBExceptionFilter
    },
    {
      provide: APP_FILTER,
      useClass: MongooseValidationErrorFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe
    },
    LoadUser
  ],
})
export class AppModule {}
