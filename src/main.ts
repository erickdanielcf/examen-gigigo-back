import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { EnvironmentVariablesService } from './config/environment/environmentVariables.service';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, { cors: true });
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  const config = app.get(EnvironmentVariablesService);
  await app.listen(config.getPort());
}
bootstrap();
