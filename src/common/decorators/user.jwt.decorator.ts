import { createParamDecorator } from '@nestjs/common';

export const UserJWT = createParamDecorator((data, req) => {
  // Los datos del usuario se asignan cuando
  // se realiza la autenticación a travez del JWT
  // return req.user;
  return data ? req.user && req.user[data] : req.user;
});
