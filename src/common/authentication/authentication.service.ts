import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserModel } from '../../api/database_management/models/user.model';
import { User } from '../../api/database_management/interfaces/user/user.interface';

@Injectable()
export class AuthenticationService {
  constructor(private readonly userModel: UserModel, private readonly jwtService: JwtService) {}

  async login(user: User) {
    return this.jwtService.sign(user.payloadToken());
  }

  async validateUser(payload: any) {
    const { email } = payload;

    return await this.userModel.findUserByEmail(email);
  }
}
