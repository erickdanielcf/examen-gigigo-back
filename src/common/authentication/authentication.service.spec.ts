import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { UserModel } from '../../api/database_management/models/user.model';
import { JwtStrategy } from './jwt.strategy';
import { AuthenticationService } from './authentication.service';
import { LanguageModule } from '../../config/languages/config/language.module';
import { EnvironmentVariablesModule } from '../../config/environment/environmentVariables.module';
import { EnvironmentVariablesService } from '../../config/environment/environmentVariables.service';
import { HttpStatus } from '@nestjs/common';

const mockUserModel = () => ({
  createUser: jest.fn(),
  findUserByEmail: jest.fn(),
  validateUser: jest.fn()
});

describe('Authentication Service', () => {
  let authenticationService;
  let userModel;
  let jwt;
  let jwtStrategy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.registerAsync({
          imports: [EnvironmentVariablesModule],
          inject: [EnvironmentVariablesService],
          useFactory(config: EnvironmentVariablesService) {
            return {
              secret: config.getSecretPrivateJWT(),
              signOptions: { expiresIn: config.getExpiresInJWT() }
            };
          }
        }),
        LanguageModule
      ],
      providers: [
        JwtStrategy,
        AuthenticationService,
        {
          provide: UserModel,
          useFactory: mockUserModel
        }
      ]
    }).compile();

    authenticationService = module.get<AuthenticationService>(AuthenticationService);
    userModel = module.get<UserModel>(UserModel);
    jwt = module.get<JwtService>(JwtService);
    jwtStrategy = module.get<JwtStrategy>(JwtStrategy);
  });

  it('Should be defined', () => {
    expect(authenticationService).toBeDefined();
    expect(userModel).toBeDefined();
    expect(jwt).toBeDefined();
  });

  it('Login authentication', async () => {
    const payload = {
      name: 'Erick',
      email: 'erickfanielcf@gmail.com',
      type: 'adminBar'
    };
    const token = jwt.sign(payload);
    const data = {
      payloadToken: () => {
        return payload;
      }
    }
    const result = await authenticationService.login(data);

    expect(result).toEqual(token);
  });

  it('Validate authentication', async () => {
    const payload = {
      name: 'Erick',
      email: 'erickfanielcf@gmail.com',
      type: 'adminBar'
    };

    userModel.findUserByEmail.mockResolvedValue(payload);

    const result = await authenticationService.validateUser(payload);

    expect(result).toEqual(payload);
  });

  it('Error Validate JwtStrategy', async () => {
    authenticationService.validateUser = jest.fn();
    authenticationService.validateUser.mockResolvedValue(null);

    const payload = {
      name: 'Erick',
      email: 'erickfanielcf@gmail.com',
      type: 'adminBar'
    };
    try {
      await jwtStrategy.validate(payload);
    } catch (error) {
      expect(error.status).toEqual(HttpStatus.UNAUTHORIZED);
    }

  });

  it('Validate JwtStrategy', async () => {
    const user = {
      'barName': 'bar de prueba',
      'email': 'barprueba@gmail.com',
      'password': '$2b$10$PsL3iX5d.Tb5ivwnGb9qHeq5eABbJ5C9t1ro72rLuCEG8XlCtd2AK'
    };
    const payload = {
      name: 'Erick',
      email: 'erickfanielcf@gmail.com',
      type: 'adminBar'
    };

    authenticationService.validateUser = jest.fn();
    authenticationService.validateUser.mockResolvedValue(user);

    const result = await jwtStrategy.validate(payload);

    expect(result).toEqual(payload);
  });

});
