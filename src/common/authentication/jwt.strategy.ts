import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt } from "passport-jwt";
import { Injectable } from "@nestjs/common";
import { AuthenticationService } from "./authentication.service";
import { UnauthorizedException } from "../../config/exceptions/unauthorized.exception";
import { EnvironmentVariablesService } from "../../config/environment/environmentVariables.service";
import { ALGORITHM_JWT } from '../constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthenticationService,
    private readonly config: EnvironmentVariablesService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      algorithms: [ALGORITHM_JWT],
      secretOrKey: config.getSecretPublicJWT()
    });
  }

  async validate(payload: any) {
    const user = await this.authService.validateUser(payload);

    if (!user) {
      throw new UnauthorizedException('unauthorized');
    }

    return payload;
  }
}