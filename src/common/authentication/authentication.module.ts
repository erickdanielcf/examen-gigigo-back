import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { DatabaseManagementModule } from '../../api/database_management/database.management.module';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { EnvironmentVariablesService } from '../../config/environment/environmentVariables.service';
import { EnvironmentVariablesModule } from '../../config/environment/environmentVariables.module';
import { ALGORITHM_JWT } from '../constants';

@Module({
  imports: [
    DatabaseManagementModule,
    PassportModule.register({
      defaultStrategy: 'jwt'
    }),
    JwtModule.registerAsync({
      imports: [EnvironmentVariablesModule],
      inject: [EnvironmentVariablesService],
      useFactory(config: EnvironmentVariablesService) {
        return {
          secret: config.getSecretPrivateJWT(),
          signOptions: {
            algorithm: ALGORITHM_JWT,
            expiresIn: config.getExpiresInJWT()
          }
        };
      }
    })
  ],
  providers: [AuthenticationService, JwtStrategy],
  exports: [AuthenticationService]
})
export class AuthenticationModule {}
