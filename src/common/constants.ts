// Security
export const ALGORITHM_JWT = 'ES512';
export const SALT_ROUNDS_HASH_PASSWORD = 10;

// Validators DTO
export const MIN_PASSWORD = 8;
export const MAX_PASSWORD = 15;