import * as moment from 'moment';
import * as mongoose from 'mongoose';

export class Query {
  private static readonly queryKeys: Set<string> = new Set([
    'skip',
    'page',
    'limit',
    'sort',
    'asc',
    'desc',
    'filter',
    'exists',
    'keyFromTo',
    'from',
    'to',
    'showMembers'
  ]);

  /**
   * Create an array for use in Aggregation Pipeline of MongoDB query.
   * @param query Object query.
   * @returns Array of arguments.
   */
  static createAggregationPipeline(query: any): any[] {
    const aggregationPipeline = [];
    const dinamicJson = {};

    for (const key in query) {
      if (!Query.queryKeys.has(key)) {
        if (query[key] === 'false') {
          dinamicJson[key] = false;
        } else if (query[key] === 'true') {
          dinamicJson[key] = true;
        } else if (mongoose.Types.ObjectId.isValid(query[key])) {
          dinamicJson[key] = mongoose.Types.ObjectId(query[key]);
        } else if (key === 'search') {
          dinamicJson[key] = new RegExp(`${query[key].toLowerCase()}`);
        } else {
          dinamicJson[key] = query[key];
        }
      }
    }

    if (query.from && query.to && query.keyFromTo) {
      dinamicJson[query.keyFromTo] = {
        $gte: moment(parseInt(query.from)).toDate(),
        $lte: moment(parseInt(query.to)).toDate()
      };
    } else if (query.from && query.keyFromTo) {
      dinamicJson[query.keyFromTo] = { $gte: moment(parseInt(query.from)).toDate() };
    } else if (query.to && query.keyFromTo) {
      dinamicJson[query.keyFromTo] = { $lte: moment(parseInt(query.to)).toDate() };
    }

    if (query.exists) {
      const activos = query.exists.split(',');

      for (const value of activos) {
        dinamicJson[value.split(':')[0]] = { $exists: value.split(':')[1] === 'true' };
      }
    }

    aggregationPipeline.push({ $match: dinamicJson });

    // Paginated section
    if (query.limit && query.page && query.page > 0 && query.limit > 0) {
      aggregationPipeline.push({ $skip: query.limit * (query.page - 1) });
    }

    // establishes the search limit in case more data is needed
    // by default all
    if (query.limit && query.limit > 0) {
      const limit = parseInt(query.limit);

      aggregationPipeline.push({ $limit: limit });
    }

    // Order query section
    let key = null;

    if (query.sort) {
      key = query.sort;
    }

    if (key && query.asc) {
      aggregationPipeline.push({ $sort: { [key]: 1 } });
    } else if (key && query.desc) {
      aggregationPipeline.push({ $sort: { [key]: -1 } });
    } else if (key) {
      aggregationPipeline.push({ $sort: { [key]: 1 } });
    }

    // Projection query section
    if (query.filter) {
      const filtro = query.filter.split(',');
      const project = {};

      for (const value of filtro) {
        project[value] = 1;
      }

      aggregationPipeline.push({ $project: project });
    }

    return aggregationPipeline;
  }
}
