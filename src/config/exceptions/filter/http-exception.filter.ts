import { ExceptionFilter, Catch, ArgumentsHost, HttpException, Logger } from '@nestjs/common';
import { Response } from 'express';
import * as i18n from 'i18n';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private logger: Logger = new Logger('HttpExceptionFilter');

  catch(exception: HttpException, host: ArgumentsHost) {
    this.logger.debug('Type Error');
    this.logger.debug(exception.constructor.name);

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    // const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    let messageError: string;

    if (exception.message.statusCode) {
      switch (exception.message.statusCode) {
        case 401:
          messageError = i18n.__('notPermissions');
          break;
        case 403:
          messageError = i18n.__('forbidden');
          break;
        default:
          messageError = exception.message.message || exception.message.error || exception.message || null;
          break;
      }
    } else if (typeof exception.message === 'string') {
      messageError = exception.message;
    } else {
      messageError = exception.message.message || exception.message.error || exception.message || null;
    }

    this.logger.error(exception.getStatus());
    this.logger.error(messageError);
    this.logger.debug(exception.stack);

    response.status(status).json({
      statusCode: status,
      // timestamp: new Date().toISOString(),
      // path: request.url,
      // message: exception.message.message || exception.message.error || exception.message || null
      message: messageError
    });
  }
}
