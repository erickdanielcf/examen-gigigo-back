import { ExceptionFilter, Catch, ArgumentsHost, Logger } from '@nestjs/common';
import { Response } from 'express';
import { MongoError } from 'mongodb';
import * as i18n from 'i18n';

@Catch(MongoError)
export class MongoDBExceptionFilter implements ExceptionFilter {
  private logger: Logger = new Logger('MongoDBExceptionFilter');

  catch(exception: MongoError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    // const request = ctx.getRequest<Request>();
    const code = exception.code;

    this.logger.error(exception.name);
    this.logger.error(exception.code);
    this.logger.error(exception.message);
    // this.logger.debug(exception.errmsg);
    // this.logger.debug(exception.stack);

    let messageError: string;

    if (code === 11000) {
      messageError = i18n.__('duplicate', {
        meta: exception.message
          .split(':')[4]
          .trim()
          .split('"')[1]
      });
    }

    response.status(400).json({
      statusCode: 400,
      // timestamp: new Date().toISOString(),
      // path: request.url,
      // message: exception.message.message || exception.message.error || exception.message || null
      message: messageError
    });
  }
}
