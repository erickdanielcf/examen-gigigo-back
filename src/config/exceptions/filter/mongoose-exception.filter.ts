import { ExceptionFilter, Catch, ArgumentsHost, Logger } from '@nestjs/common';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import * as i18n from 'i18n';

@Catch(mongoose.Error.ValidationError)
export class MongooseValidationErrorFilter implements ExceptionFilter {
  private logger: Logger = new Logger('MongooseValidationErrorFilter');

  catch(exception: mongoose.Error.ValidationError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    // const request = ctx.getRequest<Request>();
    const keysErrors = Object.keys(exception.errors);
    const message = exception.errors[keysErrors[0]].message;
    const arrayErrors = message.split(' ');
    let messageError: string;

    if (arrayErrors.length === 2) {
      messageError = i18n.__(arrayErrors[0], { meta: arrayErrors[1] });
    } else {
      messageError = i18n.__(arrayErrors[0]);
    }

    this.logger.error(messageError);

    response.status(400).json({
      statusCode: 400,
      // timestamp: new Date().toISOString(),
      // path: request.url,
      // message: exception.message.message || exception.message.error || exception.message || null
      message: messageError
    });
  }
}
