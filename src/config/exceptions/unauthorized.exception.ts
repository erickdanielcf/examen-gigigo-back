import { HttpStatus, HttpException } from '@nestjs/common';
import * as i18n from 'i18n';

export class UnauthorizedException extends HttpException {
  constructor(message?: string, meta?: string) {
    let messageError: string;

    if (message && meta) {
      messageError = i18n.__(message, { meta });
    } else if (message) {
      messageError = i18n.__(message);
    } else {
      messageError = 'unauthorized';
    }

    super(messageError, HttpStatus.UNAUTHORIZED);
  }
}
