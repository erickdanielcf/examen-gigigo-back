import { PipeTransform, Injectable, ArgumentMetadata, Logger } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { BadRequestException } from '../exceptions/bad.request.exception';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  private logger: Logger = new Logger('ValidationPipe');

  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    this.logger.log(errors);

    if (errors.length > 0) {
      // Si se requiere mas detalle usar la clase ValidationError de class-validator
      // e implementar un filtro propio para dichas excepciones
      throw new BadRequestException('badRequest');
    }

    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];

    return !types.includes(metatype);
  }
}
