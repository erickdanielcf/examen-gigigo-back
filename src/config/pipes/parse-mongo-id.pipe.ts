import { Injectable, PipeTransform, ArgumentMetadata } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { BadRequestException } from '../exceptions/bad.request.exception';

@Injectable()
export class ParseMongoIdPipe implements PipeTransform<string, mongoose.Types.ObjectId> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  transform(value: string, metadata: ArgumentMetadata): mongoose.Types.ObjectId {
    if (!mongoose.Types.ObjectId.isValid(value)) {
      throw new BadRequestException('badRequest');
    }

    return mongoose.Types.ObjectId(value);
  }
}
