import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from '@hapi/joi';

export interface EnvConfig {
  [key: string]: string;
}

export class EnvironmentVariablesService {
  private readonly envConfig: EnvConfig;

  /**
   * Load a file with configuration variables.
   * @param filePath The file path
   */
  constructor(filePath: string) {
    let config;

    if (fs.existsSync(filePath)) {
      config = dotenv.parse(fs.readFileSync(filePath));
    }

    this.envConfig = this.validateInput(config);
  }

  /**
   * validate the variables declared within the environment variables file.
   * @param envConfig
   * @returns object with configuration variables
   */
  private validateInput(envConfig: EnvConfig = {}): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'staging'])
        .default('development'),
      PORT: Joi.number().default(3000),
      LOCALES: Joi.array()
        .items(Joi.string())
        .default(['es', 'en']),
      DEFAULTLOCALE: Joi.string()
        .valid(['es', 'en'])
        .default('es'),
      MONGODB_SRV: Joi.boolean().default(false),
      DATABASE_HOST: Joi.string().required(),
      DATABASE_PORT: Joi.number(),
      DATABASE_USERNAME: Joi.string(),
      DATABASE_PASSWORD: Joi.string(),
      DATABASE_NAME: Joi.string().required(),
      SECRET_PRIVATE_JWT: Joi.string().required(),
      SECRET_PUBLIC_JWT: Joi.string().required(),
      EXPIRES_IN_JWT: Joi.string().required(),
      EXPIRES_IN_REFRESH_TOKEN_JWT: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(envConfig, envVarsSchema);

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedEnvConfig;
  }

  /**
   * Get value stored in the environment variables file.
   * @param key The variable key
   * @returns The current value
   */
  get(key: string): string {
    return this.envConfig[key];
  }

  /**
   * Get NODE_ENV value stored in the environment variables file.
   * @returns The current value for NODE_ENV
   */
  getNodeEnv(): string {
    return this.envConfig.NODE_ENV;
  }

  /**
   * Get PORT value stored in the environment variables file.
   * @returns The current value for PORT
   */
  getPort(): number {
    return Number(this.envConfig.PORT);
  }

  /**
   * Get LOCALES value stored in the environment variables file.
   * @returns The current value for LOCALES
   */
  getLocales(): string[] {
    return Array.from(this.envConfig.LOCALES);
  }

  /**
   * Get DEFAULTLOCALE value stored in the environment variables file.
   * @returns The current value for DEFAULTLOCALE
   */
  getDefaultLocale(): string {
    return this.envConfig.DEFAULTLOCALE;
  }

  /**
   * Get MONGODB_SRV value stored in the environment variables file.
   * @returns The current value for MONGODB_SRV
   */
  getMongoDBSrv(): boolean {
    return Boolean(this.envConfig.MONGODB_SRV);
  }

  /**
   * Get DATABASE_HOST value stored in the environment variables file.
   * @returns The current value for DATABASE_HOST
   */
  getDatabaseHost(): string {
    return this.envConfig.DATABASE_HOST;
  }

  /**
   * Get DATABASE_PORT value stored in the environment variables file.
   * @returns The current value for DATABASE_PORT
   */
  getDatabasePort(): number {
    return Number(this.envConfig.DATABASE_PORT);
  }

  /**
   * Get DATABASE_USERNAME value stored in the environment variables file.
   * @returns The current value for DATABASE_USERNAME
   */
  getDatabaseUsername(): string {
    return this.envConfig.DATABASE_USERNAME;
  }

  /**
   * Get DATABASE_PASSWORD value stored in the environment variables file.
   * @returns The current value for DATABASE_PASSWORD
   */
  getDatabasePassword(): string {
    return this.envConfig.DATABASE_PASSWORD;
  }

  /**
   * Get DATABASE_NAME value stored in the environment variables file.
   * @returns The current value for DATABASE_NAME
   */
  getDatabaseName(): string {
    return this.envConfig.DATABASE_NAME;
  }

  /**
   * Get SECRET_PRIVATE_JWT value stored in the environment variables file.
   * @returns The current value for SECRET_PRIVATE_JWT
   */
  getSecretPrivateJWT(): string {
    return this.envConfig.SECRET_PRIVATE_JWT;
  }

  /**
   * Get SECRET_PUBLIC_JWT value stored in the environment variables file.
   * @returns The current value for SECRET_PUBLIC_JWT
   */
  getSecretPublicJWT(): string {
    return this.envConfig.SECRET_PUBLIC_JWT;
  }

  /**
   * Get EXPIRES_IN_JWT value stored in the environment variables file.
   * @returns The current value for EXPIRES_IN_JWT
   */
  getExpiresInJWT(): string {
    return this.envConfig.EXPIRES_IN_JWT;
  }

  /**
   * Get EXPIRES_IN_REFRESH_TOKEN_JWT value stored in the environment variables file.
   * @returns The current value for EXPIRES_IN_REFRESH_TOKEN_JWT
   */
  getExpiresInRefreshTokenJWT(): string {
    return this.envConfig.EXPIRES_IN_REFRESH_TOKEN_JWT;
  }
}
